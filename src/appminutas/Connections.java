/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appminutas;

import java.sql.*;

/**
 *
 * @author mauricio
 */
public class Connections {
    String USERNAME = "root";
    String PASSWORD = "root";
    String CONN_STRING = 
            "jdbc:mysql://localhost:3306/minutas";
    protected static Connection conn = null;
    
    public Connections() {
        
        try {
            conn = DriverManager.getConnection(CONN_STRING, USERNAME, PASSWORD);
            System.out.println("Conectado correctamente");
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
