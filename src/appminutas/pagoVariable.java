/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appminutas;

import static java.lang.Float.min;

/**
 *
 * @author mauricio
 */
public class pagoVariable {
    String nombreLinea;
    float[] costo;
    float[] calidad;
    float[] servicio;
    
//  LÍNEA 1
    static float[] costoLinea1, calidadLinea1 ,servicioLinea1;
    
//  CKF
    static float [] costoCKF, calidadCKF, servicioCKF;
    
    //  DTC 3000
    static float [] costoD3, calidadD3, servicioD3;
   
//  DTC 6000
    static float [] costoD6, calidadD6, servicioD6;
    
    
//  FCC  
    static float [] costoF, calidadF, servicioF;
    
//  CHEETOS  
    static float [] costoC, calidadC, servicioC;
    
    public pagoVariable() {
        nombreLinea = "SN";
        costo = new float[] {0.0f, 0.0f, 0.0f};
        calidad = new float[] {0.0f, 0.0f, 0.0f};
        servicio = new float[] {0.0f, 0.0f, 0.0f};
        
        costoLinea1 = new float[] {83.5f, 85.1f, 86.5f};
        calidadLinea1 = new float[] {72.5f, 77.8f, 83.3f};
        servicioLinea1 = new float[] {95.0f, 97.0f, 99.0f};
        
        costoCKF = new float[] {89.0f, 92.0f, 95.0f};
        calidadCKF = new float[] {75.0f, 82.5f, 87.5f};
        servicioCKF= new float[] {95.0f, 98.0f, 99.0f};
        
        costoD3 = new float[] {89.3f, 90.3f, 91.2f};
        calidadD3 = new float[] {78.8f, 81.8f, 87.5f};
        servicioD3 = new float[] {96.5f, 97.5f, 99.0f};
        
        costoD6 = new float[] {89.9f, 91.0f, 92.1f};
        calidadD6 = new float[] {77.8f, 81.3f, 87.5f};
        servicioD6 = new float[] {95.0f, 97.0f, 99.0f};
        
        costoF = new float[] {86.9f, 88.1f, 89.7f};
        calidadF = new float[] {79.0f, 81.3f, 85.3f};
        servicioF = new float[] {95.0f, 97.5f, 99.0f};
        
        costoC = new float[] {95.0f, 96.4f, 97.3f};
        calidadC = new float[] {81.3f, 86.3f, 91.3f};
        servicioC = new float[] {95.0f, 97.5f, 99.5f};
    }
    
   
}
